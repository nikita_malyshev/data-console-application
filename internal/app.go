package app

import (
	"fmt"
	"os"
)

func Run() {
	for {
		fmt.Println("\nВыберите действие:")
		fmt.Println("1. Создать компьютер")
		fmt.Println("2. Вывести список компьютеров")
		fmt.Println("3. Обновить компьютер")
		fmt.Println("4. Удалить компьютер")
		fmt.Println("0. Выйти")

		var choice string
		fmt.Print("Введите номер действия: ")
		fmt.Scanln(&choice)

		switch choice {
		case "1":
			createComputer()
		case "2":
			listComputers()
		case "3":
			updateComputer()
		case "4":
			deleteComputer()
		case "0":
			fmt.Println("Выход из программы.")
			os.Exit(0)
		default:
			fmt.Println("Некорректный выбор. Пожалуйста, выберите существующее действие.")
		}
	}
}
