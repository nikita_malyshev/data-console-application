package app

import "fmt"

type Computer struct {
	ID     int
	CPU    string
	GPU    string
	RAM    int
	Memory int
}

var computers []Computer
var computerIDCounter int

func createComputer() {
	computerIDCounter++
	computer := Computer{
		ID:     computerIDCounter,
		CPU:    getInput("Введите модель CPU: "),
		GPU:    getInput("Введите модель GPU: "),
		RAM:    getIntegerInput("Введите объем RAM (в GB): "),
		Memory: getIntegerInput("Введите объем Memory (в GB): "),
	}
	computers = append(computers, computer)
	fmt.Printf("Компьютер #%d создан: CPU: %s, GPU: %s, RAM: %d GB, Memory: %d GB\n",
		computer.ID, computer.CPU, computer.GPU, computer.RAM, computer.Memory)
}

func listComputers() {
	if len(computers) == 0 {
		fmt.Println("Список компьютеров пуст.")
		return
	}

	fmt.Println("Список компьютеров:")
	for _, computer := range computers {
		fmt.Printf("#%d: CPU: %s, GPU: %s, RAM: %d GB, Memory: %d GB\n",
			computer.ID, computer.CPU, computer.GPU, computer.RAM, computer.Memory)
	}
}

func updateComputer() {
	id := getIntegerInput("Введите ID компьютера для обновления: ")
	for i, computer := range computers {
		if computer.ID == id {
			computers[i].CPU = getInput("Введите новую модель CPU: ")
			computers[i].GPU = getInput("Введите новую модель GPU: ")
			computers[i].RAM = getIntegerInput("Введите новый объем RAM (в GB): ")
			computers[i].Memory = getIntegerInput("Введите новый объем Memory (в GB): ")
			fmt.Printf("Компьютер #%d обновлен: CPU: %s, GPU: %s, RAM: %d GB, Memory: %d GB\n",
				id, computers[i].CPU, computers[i].GPU, computers[i].RAM, computers[i].Memory)
			return
		}
	}
	fmt.Printf("Компьютер #%d не найден.\n", id)
}

func deleteComputer() {
	id := getIntegerInput("Введите ID компьютера для удаления: ")
	for i, computer := range computers {
		if computer.ID == id {
			computers = append(computers[:i], computers[i+1:]...)
			fmt.Printf("Компьютер #%d удален.\n", id)
			return
		}
	}
	fmt.Printf("Компьютер #%d не найден.\n", id)
}

func getInput(prompt string) string {
	var input string
	fmt.Print(prompt)
	fmt.Scanln(&input)
	return input
}

func getIntegerInput(prompt string) int {
	var input int
	fmt.Print(prompt)
	fmt.Scanln(&input)
	return input
}
