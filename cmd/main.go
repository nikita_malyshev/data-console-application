package main

import (
	"fmt"

	app "gitlab.com/nikita_malyshev/data-console-application/internal"
)

func main() {
	fmt.Println("CRUD- console application")

	app.Run()
}
